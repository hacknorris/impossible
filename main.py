#!/usr/bin/env python3

from random import randint
import time
import os
import sys
from PIL import Image
import glob
import math

levels = {
    "1":[1,5],
    "2":[0.5,2.5],
    "3":[0.2,1],
    "4":[0.1,0.5],
    "5":[0.1,0.2]
}

dimensions = os.get_terminal_size() #columns

colors = ["█","▓","▒","░"," "]

line = "\n"+("="*dimensions.columns) + "\n" + colors[4]*dimensions.columns

from brick import current
from brick import var as thing
time.sleep(randint(1,5)/randint(10,100))
os.kill(current, 9)
sprite_size = 16
sprites = []
for filename in glob.glob('./images/*.png'):
    if filename.startswith("enemy"):
        im=Image.open(filename).rotate(-90)
    else:
        im=Image.open(filename).rotate(90)
    sprites.append(im)

level = int(input(line+"select level - from 1 (easiest) to 5 (hardest):\n"))

user = int(input(line+"enter number from 0 to 9.9:\n"))

number = int(repr(thing)[0]) + int(repr(thing)[-1])*0.1

if user < 0 or user > 9.9:
    print("WTF YOU TYPED???")

shooted = []
close = []
enemypic = []

match level:
    case 1:
        shooted = levels["1"][0]
        close = levels["1"][1]
        print(sprites)
        enemypic = sprites[0]
    case 2:
        shooted = levels["2"][0]
        close = levels["2"][1]
        enemypic = sprites[1]
    case 3:
        shooted = levels["3"][0]
        close = levels["3"][1]
        enemypic = sprites[2]
    case 4:
        shooted = levels["4"][0]
        close = levels["4"][1]
        enemypic = sprites[3]
    case 5:
        shooted = levels["5"][0]
        close = levels["5"][1]
        enemypic = sprites[4]
    case default:
        print("invalid number")

if abs(int(user) - number) <= shooted:
    new = Image.new("RGB", (dimensions.columns,16))
    new.paste(enemypic,(math.floor(dimensions.columns/4),0))
    new.paste(sprites[10],(math.floor(dimensions.columns/2)+16,0))
    for i in new.getdata():
        banner = colors[(sum(i)//150)-1]

    new_pixels_count = len(banner)
    ascii_image = [banner[index:index + dimensions.columns] for index in range(0, new_pixels_count, dimensions.columns)]
    ascii_image = "\n".join(ascii_image)
    bann = ascii_image
    print(line+bann+line+"\nyou won!!!"+line+"\nship was "+str(number)+" L.Y. away")
    os.execv(sys.argv[0], sys.argv)
    quit()
elif abs(int(user) - number) <= close:
    new = Image.new("RGB", (dimensions.columns,16))
    new.paste(enemypic,(math.floor(dimensions.columns/4),0))
    new.paste(sprites[10],(math.floor(dimensions.columns/2)+16,0))
    for i in new.getdata():
        banner = colors[(sum(i)//150)-1]

    new_pixels_count = len(banner)
    ascii_image = [banner[index:index + dimensions.columns] for index in range(0, new_pixels_count, dimensions.columns)]
    ascii_image = "\n".join(ascii_image)
    bann = ascii_image
    print(line+bann+line+"\nclose enough…"+line+"\nship was "+str(number)+" L.Y. away")
    os.execv(sys.argv[0], sys.argv)
    quit()
else:
    new = Image.new("RGB", (dimensions.columns,16))
    new.paste(enemypic,(math.floor(dimensions.columns/4),0))
    new.paste(sprites[10],(math.floor(dimensions.columns/2)+16,0))
    for i in new.getdata():
        banner = colors[(sum(i)//150)-1]

    banner = ''.join(banner)
    new_pixels_count = len(banner)
    ascii_image = [banner[index:index + dimensions.columns] for index in range(0, new_pixels_count, dimensions.columns)]
    ascii_image = "\n".join(ascii_image)
    bann = ascii_image
    print(line+bann+line+"\nepic hacker fail >_<\nthey haven't even seen you…"+line+"\nship was "+str(number)+" L.Y. away"+line)
    os.execv(sys.argv[0], sys.argv)
    quit()
