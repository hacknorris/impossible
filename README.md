# impossible

there's been impossible games. but all are playable - just hard. now you got a game which by-design won't work on 99.9% devices. i bet you can't play this. 😈

if you can play this somehow and your machine won't die - please message me with screenshot of your score

requirements: basic python3 + PIL + unix/linux-based OS + a strong AF computer (no, not yours)

[my first email](mailto:hacknorris@tutanota.com) - in case if your boss fires you after this game shutted down servers (more encrypted)

[my second email](mailto:hacknorris-dev@proton.me) - in case you somehow ran this and want to share score
